function userLockscreenForm() {
    // Выпадающий логин
    // Fix input element click problem
//    $('.dropdown input, .dropdown label').click(function(e) {
//        e.stopPropagation();
//    });

    // Ajax для логина
    var $lockscreenForm = $('#lockscreenForm');
    var $validationMessage = $lockscreenForm.find('#validationMessage');

    function authAjaxCallback(response, result, jXHR) {
        if ('undefined' == typeof response.code || -1 == response.code) {
            $lockscreenForm.addClass('has-error');
            $validationMessage.text(response.data);
            $validationMessage.removeClass('hidden');
        }

        if (1 == response.code) {
            location.href = mainPageUrl;
        }
    }

    function submitCallback(event) {
        event.preventDefault();

        $.post('auth/login', $(this).serialize(), authAjaxCallback);
    }

    $lockscreenForm.on('submit', submitCallback);
}


$(function () {
    userLockscreenForm();
});
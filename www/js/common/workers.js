function renderBackendValidatorResultIfExists() {
    if (typeof validatorResult == 'undefined') {
        return false;
    }
    var popoverSettings = {
//        'trigger'   : 'manual',
        'content'  : 'error',
        'placement': 'auto left',
        'container': 'body'
    };
    jQuery.each(validatorResult, function (field, message) {
        popoverSettings.content = message;
        $('input[name$="[' + field + ']"]').popover('destroy').popover(popoverSettings).popover('show');
    });
}

function userLoginPopupForm() {
    // Выпадающий логин
    // Fix input element click problem
//    $('.dropdown input, .dropdown label').click(function(e) {
//        e.stopPropagation();
//    });

    // Ajax для логина
    var $loginForm = $('#loginForm');
    var $loginFormAlertBox = $loginForm.find('#loginFormAlertBox');

    function loginAjaxCallback(response, result, jXHR) {
        if ('undefined' == typeof response.code || -1 == response.code) {
            $loginForm.addClass('has-error');
            $('#errorMessage').text(response.data);
            $loginFormAlertBox.removeClass('hidden');
        }

        if (1 == response.code) {
            location.reload();
        }
    }

    function submitCallback(event) {
        event.preventDefault();

        $.post('/auth/login', $(this).serialize(), loginAjaxCallback);
    }

    $loginForm.on('submit', submitCallback);
    $loginFormAlertBox.on('click', 'button.close', function (event) {
        event.stopPropagation();
        $(event.delegateTarget).addClass('hidden');
    });
}

function initValidatorRules(formSelector, rulesAndMessges, transferSettings) {
    const ERROR_FINDING_CLASS_NAME = 'errorPopover';

    const TRANSFER_SMART = 'smartLayout';
    const TRANSFER_POPOVER = 'popover';

    rulesAndMessges = typeof rulesAndMessges !== 'undefined' ? rulesAndMessges : {};
    transferSettings = typeof transferSettings !== 'undefined' ? transferSettings : TRANSFER_SMART;


    var showErrorsHandlerViaPopover = function (errorMap, errorList) {
        for (var i = 0; i < errorList.length; i++) {
            var item = errorList[i];
            if (this.settings.highlight) {
                this.settings.highlight.call(this, item);
            }
        }
        if (this.settings.unhighlight) {
            for (var i = 0, elements = this.validElements(); elements[i]; i++) {
                this.settings.unhighlight.call(this, elements[i], this.settings.errorClass, this.settings.validClass);
            }
        }
    };

    var viaSettings = {};
    viaSettings[TRANSFER_SMART] = {
        errorClass    : "error",
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent());
        }
    };
    viaSettings[TRANSFER_POPOVER] = {
        'errorClass' : "invalid",
        'highlight'  : function (errorItem) {
            var popoverSettings = {
                'trigger'  : 'manual',
                'content'  : 'error',
                'placement': 'auto top',
                'container': 'body'
            };
            popoverSettings.content = $.isFunction(errorItem.message) ? errorItem.message() : errorItem.message;
            $(errorItem.element).addClass(ERROR_FINDING_CLASS_NAME).popover('destroy').popover(popoverSettings).popover('show');
        },
        'unhighlight': function (element, errorClass, validClass) {
            $(element).popover('destroy');
        },
        'showErrors' : showErrorsHandlerViaPopover
        //'submitHandler' : that.submitHandler
    };

    var transferAdapterSettings = typeof viaSettings[transferSettings] !== 'undefined' ? viaSettings[transferSettings] : viaSettings[TRANSFER_SMART];
    var validatorSettings = $.extend(transferAdapterSettings, rulesAndMessges);


    return $(formSelector).validate(validatorSettings);
}

function attachSummernoteToElementInForm(formSelector, textareaSelector, imageUploadUrl) {
    var imageUploadUrl = typeof imageUploadUrl !== 'undefined' ? imageUploadUrl : '/cp/files/blackhole';

    function sendFile(file, editor, welEditable) {
        var data = new FormData();
        data.append('qqfile', file);
        $.ajax({
            data       : data,
            type       : 'POST',
            url        : imageUploadUrl,
            cache      : false,
            contentType: false,
            processData: false,
            success    : function (url) {
                editor.insertImage(welEditable, url);
            }
        });
    }

    var $form = $(formSelector),
        $textareaElement = $form.find(textareaSelector)
        ;
    if (!$textareaElement.size()) {
        throw new ReferenceError("Textarea '{T}' has not beed found in form '{F}'", {"T": textareaSelector, "F": formSelector});
    }

    var summernoteId = $textareaElement.attr('id') + 'Summernote',
        $summernoteContainer = $('<div></div>')
            .addClass('summernote')
            .attr('id', summernoteId)

        ;
    $textareaElement.after($summernoteContainer);
    $textareaElement.addClass('hidden');

    $summernoteContainer.summernote({
        height       : 180,
        focus        : false,
        onImageUpload: function (files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        },
        tabsize      : 2
    });

    $summernoteContainer.code($textareaElement.val()); // инициализируем начальное значение у самерноута

    function onFormSubmit(event) {
        $textareaElement.val($summernoteContainer.code());

        return true;
    }

    $form.bindFirst('submit', onFormSubmit); // должен быть первым, иначе можно подзаебаться искать ошибки или думать как соединить jquery.validate и этот саммерноут

    return $summernoteContainer;
}

function attachAjaxErrorHandler() {
    var modalTemplate = '<div class="modal" id="ajaxErrorResponseModalContainer" tabindex="-1" role="dialog" aria-labelledby="remoteModalAjaxErrorLabel" aria-hidden="true">\n  <div class="modal-dialog modal-lg">\n    <div class="modal-content"></div>\n  </div>\n</div>';
    var $modalAjaxErrorContainer = $('#ajaxErrorResponseModalContainer');

    if (!$modalAjaxErrorContainer.size()) {
        $modalAjaxErrorContainer = $(modalTemplate);
        $modalAjaxErrorContainer.appendTo($.root_.app);
    }

    var $modalContent = $modalAjaxErrorContainer.find('.modal-content');


    function ajaxErrorCallback(event, jqxhr, settings, thrownError) {
        var jsonData = $.parseJSON(jqxhr.responseText);
        var code = typeof jsonData.code !== 'undefined' ? parseInt(jsonData.code) : parseInt(0);
        var error = typeof jsonData.error !== 'undefined' ? parseInt(jsonData.error) : parseInt(0);
        var hasError = error > 0 ? true : false;

        if (!hasError) {
            return false;
        }

        if (typeOf(jsonData.error_html_template) !== 'undefined') {
            $modalContent.html(jsonData.error_html_template);
            $modalAjaxErrorContainer.modal();
        }

    }

    $(document).ajaxError(ajaxErrorCallback);

    $.root_.on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
}

function Notification() {
    var _this = this;

    var colors = {
        'success': '#739e73',
        'error'  : '#C46A69',
        'warning': '#C79121',
        'info'   : '#3276B1'
    };

    this.color = colors.warning;
    this.colors = colors;

    this.showMessage = function (title, message, color) {
        color = typeof color !== 'undefined' ? color : _this.color;

        $.smallBox({
            title    : title,
            sound    : false,
            content  : message,
            color    : color,
            iconSmall: "fa fa-exclamation-triangle",
            timeout  : 9000
        });
    };
}

/*
 * SMART ACTIONS
 */
var saActions = {

    // LOGOUT MSG
    userLogout      : function ($this) {

        // ask verification
        $.SmartMessageBox({
            title  : "<i class='fa fa-sign-out txt-color-orangeDark'></i> Logout <span class='txt-color-orangeDark'><strong>" + $('#show-shortcut').text() + "</strong></span> ?",
            content: $this.data('logout-msg') || "You can improve your security further after logging out by closing this opened browser",
            buttons: '[No][Yes]'

        }, function (ButtonPressed) {
            if (ButtonPressed == "Yes") {
                $.root_.addClass('animated fadeOutUp');
                setTimeout(logout, 1000);
            }
        });
        function logout() {
            window.location = $this.attr('href');
        }

    },

    // RESET WIDGETS
    resetWidgets    : function ($this) {
        $.widresetMSG = $this.data('reset-msg');

        $.SmartMessageBox({
            title  : "<i class='fa fa-refresh' style='color:green'></i> Clear Local Storage",
            content: $.widresetMSG || "Would you like to RESET all your saved widgets and clear LocalStorage?",
            buttons: '[No][Yes]'
        }, function (ButtonPressed) {
            if (ButtonPressed == "Yes" && localStorage) {
                localStorage.clear();
                location.reload();
            }

        });
    },

    // LAUNCH FULLSCREEN
    launchFullscreen: function (element) {

        if (!$.root_.hasClass("full-screen")) {

            $.root_.addClass("full-screen");

            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }

        } else {

            $.root_.removeClass("full-screen");

            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }

        }

    },

    // MINIFY MENU
    minifyMenu      : function () {
        if (!$.root_.hasClass("menu-on-top")) {
            $.root_.toggleClass("minified");
            $.root_.removeClass("hidden-menu");
            $('html').removeClass("hidden-menu-mobile-lock");
            $this.effect("highlight", {}, 500);
        }
    },

    // TOGGLE MENU
    toggleMenu      : function () {
        if (!$.root_.hasClass("menu-on-top")) {
            $('html').toggleClass("hidden-menu-mobile-lock");
            $.root_.toggleClass("hidden-menu");
            $.root_.removeClass("minified");
        } else if ($.root_.hasClass("menu-on-top") && $.root_.hasClass("mobile-view-activated")) {
            $('html').toggleClass("hidden-menu-mobile-lock");
            $.root_.toggleClass("hidden-menu");
            $.root_.removeClass("minified");
        }
    },

    // TOGGLE SHORTCUT
    toggleShortcut  : function () {

        if ($.shortcut_dropdown.is(":visible")) {
            shortcut_buttons_hide();
        } else {
            shortcut_buttons_show();
        }

        // SHORT CUT (buttons that appear when clicked on user name)
        $.shortcut_dropdown.find('a').click(function (e) {
            e.preventDefault();
            window.location = $(this).attr('href');
            setTimeout(shortcut_buttons_hide, 300);

        });

        // SHORTCUT buttons goes away if mouse is clicked outside of the area
        $(document).mouseup(function (e) {
            if (!$.shortcut_dropdown.is(e.target) && $.shortcut_dropdown.has(e.target).length === 0) {
                shortcut_buttons_hide();
            }
        });

        // SHORTCUT ANIMATE HIDE
        function shortcut_buttons_hide() {
            $.shortcut_dropdown.animate({
                height: "hide"
            }, 300, "easeOutCirc");
            $.root_.removeClass('shortcut-on');

        }

        // SHORTCUT ANIMATE SHOW
        function shortcut_buttons_show() {
            $.shortcut_dropdown.animate({
                height: "show"
            }, 200, "easeOutCirc");
            $.root_.addClass('shortcut-on');
        }

    }

};


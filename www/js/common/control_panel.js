$(function() {
    if (typeof(pageSetUp) != 'undefined') {
        pageSetUp();
    }

    if (typeof(attachAjaxErrorHandler) != 'undefined') {
        attachAjaxErrorHandler();
    }
});
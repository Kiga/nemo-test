function translit(target, goal) {
    var space = '-';
    var text = $(target).val().toLowerCase();

    var transl = {
        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
        'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
        'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
        'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': space, 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya',
        ' ': space, '_': space, '`': space, '~': space, '!': space, '@': space,
        '#': space, '$': space, '%': space, '^': space, '&': space, '*': space,
        '(': space, ')': space, '-': space, '\=': space, '+': space, '[': space,
        ']': space, '\\': space, '|': space, '/': space, '.': space, ',': space,
        '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
        '?': space, '<': space, '>': space, '№': space
    }

    var result = '';
    var curent_sim = '';

    for (var i = 0; i < text.length; i++) {
        if (transl[text[i]] != undefined) {
            if (curent_sim != transl[text[i]] || curent_sim != space) {
                result += transl[text[i]];
                curent_sim = transl[text[i]];
            }
        }
        else {
            result += text[i];
            curent_sim = text[i];
        }
    }

    result = TrimStr(result);

    $(goal).val(result);

}
function TrimStr(str) {
    str = str.replace(/^-/, '');
    return str.replace(/-$/, '');
}
function pluralString(number, form1, form2, form3) {
    //closure
    function plural(a) {
        if (a % 10 == 1 && a % 100 != 11) {
            return 0;
        } else if (a % 10 >= 2 && a % 10 <= 4 && ( a % 100 < 10 || a % 100 >= 20)) {
            return 1
        } else {
            return 2;
        }
    }

    switch (plural(number)) {
        case 0 :
            return form1;
        case 1 :
            return form2;
        default:
            return form3;
    }
}
function withVars(func, some_more_args) {
    var args = Array.prototype.slice.call(arguments, 1); //для передачи группы аргументов
    return function () {
        var calcArgs = new Array();
        jQuery.each(args, function (position, value) {
            calcArgs.push(jQuery.isFunction(value) ? value() : value);
        });
        if (jQuery.isFunction(args[0]) && 1 == args.length) {
            calcArgs = args[0]();
        }
        return func.apply(null, args.slice.call(arguments, 0).concat(calcArgs)); //сцепляем 2 группы аргументов, жёстко мутишь ))
    }
}
function typeOf(value) {
    var s = typeof value;
    if (s === 'object') {
        if (value) {
            if (Object.prototype.toString.call(value) == '[object Array]') {
                s = 'array';
            }
        } else {
            s = 'null';
        }
    }
    return s;
}
function isEmpty(o) {
    var i, v;
    if (typeOf(o) === 'object') {
        for (i in o) {
            v = o[i];
            if (v !== undefined && typeOf(v) !== 'function') {
                return false;
            }
        }
    }
    return true;
}
if (!String.prototype.entityify) {
    String.prototype.entityify = function () {
        return this.replace(/&/g, "&amp;").replace(/</g,
            "&lt;").replace(/>/g, "&gt;");
    };
}

if (!String.prototype.quote) {
    String.prototype.quote = function () {
        var c, i, l = this.length, o = '"';
        for (i = 0; i < l; i += 1) {
            c = this.charAt(i);
            if (c >= ' ') {
                if (c === '\\' || c === '"') {
                    o += '\\';
                }
                o += c;
            } else {
                switch (c) {
                    case '\b':
                        o += '\\b';
                        break;
                    case '\f':
                        o += '\\f';
                        break;
                    case '\n':
                        o += '\\n';
                        break;
                    case '\r':
                        o += '\\r';
                        break;
                    case '\t':
                        o += '\\t';
                        break;
                    default:
                        c = c.charCodeAt();
                        o += '\\u00' + Math.floor(c / 16).toString(16) +
                        (c % 16).toString(16);
                }
            }
        }
        return o + '"';
    };
}

if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(
            /\{([^{}]*)\}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

if (!String.prototype.trim) {
    String.prototype.trim = function () {
        return this.replace(/^\s*(\S*(?:\s+\S+)*)\s*$/, "$1");
    };
}

if (window.jQuery) {
    if ("function" !== typeof jQuery.fn.bindFirst) {
        jQuery.fn.bindFirst = function (name, fn) {
            /*
             [name] is the name of the event "click", "mouseover", ..
             same as you'd pass it to bind()
             [fn] is the handler function
             bind as you normally would
             don't want to miss out on any jQuery magic
             */
            this.on(name, fn);

            // Thanks to a comment by @Martin, adding support for
            // namespaced events too.
            this.each(function () {
                var handlers = $._data(this, 'events')[name.split('.')[0]];
                // take out the handler we just inserted from the end
                var handler = handlers.pop();
                // move it at the beginning
                handlers.splice(0, 0, handler);
            });
        };
    }

    if ("function" !== typeof jQuery.fn.doOnce) {
        jQuery.fn.doOnce = function (func) {
            this.length && func.apply(this);

            return this;
        }
    }
}
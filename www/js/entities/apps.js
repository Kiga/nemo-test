//sorry for DRY, i'm hurry
function getAccountByEmail(email, fieldName) {
    if ('undefined' == typeOf(accountList)) {
        return false;
    }
    var fieldName = typeof fieldName !== 'undefined' ? fieldName : 'all-that-shit';
    var result = $.grep(accountList, function (node, key) {
        return (node.email == email);
    });
    result = result.length > 0 ? result[0] : result;
    result = typeof result[fieldName] !== 'undefined' ? result[fieldName] : result;

    return result;
}
function getAccountById(id, fieldName) {
    if ('undefined' == typeOf(accountList)) {
        return false;
    }
    var fieldName = typeof fieldName !== 'undefined' ? fieldName : 'all-that-shit';
    var result = $.grep(accountList, function (node, key) {
        return (node.id == id);
    });
    result = result.length > 0 ? result[0] : result;
    result = typeof result[fieldName] !== 'undefined' ? result[fieldName] : result;

    return result;
}

var otable;
var xMessage = new Notification();
function initializeDataTable() {
    function formatPercent(value) {
        var percents = value * 100;

        return percents.toFixed(2) + '%';
    }

    function formatDollars(value) {
        var dollars = parseFloat(value);

        return '$' + dollars.toFixed(2);
    }

    function formatAccountName(accountId) {
        return getAccountById(accountId, 'email');
    }

    function setExtraDataToAjaxCall(data) {
        var $filterAccounts = $('#filterAccounts'),
            $filterDateStart = $('#filterDateStart'),
            $filterDateFinish = $('#filterDateFinish')
            ;

        var emails = [];
        var accounts = [];
        var emailList = $filterAccounts.val();
        for (var i in  emailList) {
            var emailValue = emailList[i];
            accounts.push(getAccountByEmail(emailValue, 'id'));
            emails.push(emailValue);
        }
        data.filterExtra = {
            "accounts"  : accounts,
            "dateStart" : $filterDateStart.val(),
            "dateFinish": $filterDateFinish.val(),
            "emails"    : emails
        };
    }

    $.extend($.fn.dataTableExt.oStdClasses, {"sFilter": "dataTables_filter_wide"});

    otable = $('#appsDataTable').DataTable({
        //"bFilter": false,
        //"bInfo": false,
        //"bLengthChange": false
        //"bAutoWidth": false,
        //"bPaginate": false,
        "order"     : [],
        "bStateSave": true, // saves sort state using localStorage
        "pageLength": 20,
        "lengthMenu": [[5, 15, 20, 25, 50, 100, 150, -1], [5, 15, 20, 25, 50, 100, 150, "All"]],
        "serverSide": true,
        "processing": true,
        "ajax"      : {
            "url"    : "apps/get-apps-paginate",
            "type"   : "POST",
            "dataSrc": "subset",
            "data"   : setExtraDataToAjaxCall
        },
        "columns"   : [
            {
                "data"  : "accounts_id",
                "render": formatAccountName
            },
            {"data": "app"},
            {"data": "allInstalls"},
            {"data": "impressionsDelivered"},
            {"data": "clicksDelivered"},
            {
                "data"  : "ctrDelivered",
                "render": formatPercent
            },
            {"data": "installDelivered"},
            {
                "data"  : "installRateDelivered",
                "render": formatPercent
            },
            {
                "data"  : "moneyEarned",
                "render": formatDollars
            },
            {
                "data"  : "ecpmEarned",
                "render": formatDollars
            },
            {"data": "dt_format"}
        ],
//        "createdRow": function (row, data, index) {
////            var appId = data._id.$id;
//            $(row).data('app_id', data._id.$id);
//        },
        "language"  : {
            "processing": '<span><i class="fa fa-gear fa-2x fa-spin"></i>&nbsp;Processing...</span>'
        }
    });

    function applyFilter(event) {
        otable
            .column($(this).parent().index() + ':visible')
            .search(this.value)
            .draw();
    }

    function renderSummary(e, settings, json) {
        var summary = json.summary;
        var $totalContainer = $('#totalAndSummaryContainer');
        var $summaryContainer = $('#accountSummaryContainer');
        var $totalMoney = $('#totalMoney');

        $summaryContainer.empty();
        for (var accountId in summary) {
            var emailValue = getAccountById(accountId, 'email');
            var summaryValue = summary[accountId].moneyEarned;

            var boxSumm = '<div>\n  <div class="font-md">\n    <strong>{email} :</strong>\n    <span class="pull-right"> {sum} </span>\n  </div>\n</div>';
            boxSumm = boxSumm.supplant({
                "email": emailValue,
                "sum"  : formatDollars(summaryValue)
            });
            $summaryContainer.append(boxSumm);
            //console.log(accountId);console.log(summaryValue);console.log(emailValue);
        }
        $totalMoney.html(json.summary_total.toFixed(2));
        $totalContainer.show();
    }

    // E V E N T S
    //$("#appsDataTable thead th input[type=text]").on('keyup change', applyFilter);

    otable.on('xhr.dt', renderSummary);
}
function initializeFilters() {
    var $filterAccounts = $('#filterAccounts'),
        $filterDateStart = $('#filterDateStart'),
        $filterDateFinish = $('#filterDateFinish')
        ;
    $filterDateStart.datepicker({
        //dateFormat : 'yy-mm-dd',
        dateFormat    : 'mm/dd/yy',
        defaultDate   : "+1w",
        changeMonth   : true,
        numberOfMonths: 3,
        prevText      : '<i class="fa fa-chevron-left"></i>',
        nextText      : '<i class="fa fa-chevron-right"></i>',
        onClose       : function (selectedDate) {
            $filterDateFinish.datepicker("option", "minDate", selectedDate);
        }
    });

    $filterDateFinish.datepicker({
        //dateFormat : 'yy-mm-dd',
        dateFormat    : 'mm/dd/yy',
        defaultDate   : "+1w",
        changeMonth   : true,
        numberOfMonths: 3,
        prevText      : '<i class="fa fa-chevron-left"></i>',
        nextText      : '<i class="fa fa-chevron-right"></i>',
        onClose       : function (selectedDate) {
            $filterDateStart.datepicker("option", "maxDate", selectedDate);
        }
    });
    $filterDateStart.datepicker("option", "maxDate", $filterDateFinish.val());
    $filterDateFinish.datepicker("option", "minDate", $filterDateStart.val());

    $filterAccounts
        .add($filterDateFinish)
        .add($filterDateStart)
        .on('change', function () {
            otable.draw();
        });
}

function initializeReportMechanism() {
    var $commonFilterForm = $('#commonFilterForm');
    var $btnRunReport = $('#btnRunReport');
    var $runReportAnimation = $('#runReportAnimationContainer');
    var $runReportText = $('#runReportDefaultText');
    var $modalContainer = $('#errorResponseModalContainer');
    var $modalContainerContent = $modalContainer.find('.modal-content');

    function gettingErrorAjaxResponse(response, textStatus, jqXH) {

        var jsonData = response.responseJSON;
        var code = typeof jsonData.code !== 'undefined' ? parseInt(jsonData.code) : parseInt(0);
        var error = typeof jsonData.error !== 'undefined' ? parseInt(jsonData.error) : parseInt(0);
        var hasError = error > 0 ? true : false;

        if (!hasError) {
            return false;
        }

        if (typeOf(jsonData.error_html_template) !== 'undefined') {
            $modalContainerContent.html(jsonData.error_html_template);
            $modalContainer.modal();
        }
    }

    function onFormSubmitHandler(event) {
        event.preventDefault();
        var ajaxDeferred = $.ajax({
            data      : $(this).serialize(),
            type      : 'POST',
            url       : 'apps/scrap-data',
            cache     : false,
            beforeSend: function (xhr) {
                $btnRunReport.attr('disabled', 'disabled');
                $runReportText.hide();
                $runReportAnimation.show();
            }
        });
        ajaxDeferred.always(function (data, textStatus, jqXH) {
            $runReportText.show();
            $runReportAnimation.hide();
            $btnRunReport.removeAttr('disabled');
        });
        ajaxDeferred.done(function (response, textStatus, jqXH) {
            var code = typeof response.code !== 'undefined' ? parseInt(response.code) : parseInt(0);

            if ("success" == textStatus && code > 0) {
                otable.draw();

                if (code === 3 && typeOf(response.credentials) !== 'undefined') {
                    var wrongAuthData = response.credentials;
                    var title = typeof response.message !== 'undefined' ? response.message : 'An error occured';
                    var message = 'Next data have been skipped from scrapping:<br/><br/>';
                    for (var j in wrongAuthData) {
                        var authTuple = wrongAuthData[j];
                        message += 'E-mail: <strong>' + authTuple.email + '</strong><br/>' +
                        'Password: <strong>' + authTuple.password + '</strong><br/>';
                    }

                    return xMessage.showMessage(title, message, xMessage.colors.info);
                }
            }
        });
        ajaxDeferred.fail(gettingErrorAjaxResponse);
    }

    //EVENTS
    $commonFilterForm.on('submit', onFormSubmitHandler);
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });
}
$(document).ready(function () {
    initializeDataTable();
    initializeFilters();
    initializeReportMechanism();
});

//accountSummaryContainer
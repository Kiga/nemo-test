<?php

use DB\Jig;
use DB\Mongo;
use DB\SQL;
use Helpers\MenuViewHelper;
use Models\Useful\MapperProxy;
use Zend\View\Helper\HeadMeta;
use Zend\View\Helper\HeadTitle;

/**
 * Class Bootstrap
 */
class Bootstrap extends Prefab
{
    /**
     *
     */
    const USE_STORAGE = 'use_storage';

    /**
     *
     */
    const BACKEND_ADAPTER = 'db';
    /**
     *
     */
    const BACKEND_ADAPTER_SQL = 'sql';
    /**
     *
     */
    const BACKEND_ADAPTER_FILES = 'jig';
    /**
     *
     */
    const BACKEND_ADAPTER_NOSQL = 'mongo';

    /**
     * @var array
     */
    public static $backendTypes = [self::BACKEND_ADAPTER_SQL, self::BACKEND_ADAPTER_FILES, self::BACKEND_ADAPTER_NOSQL];

    /**
     * @var array
     */
    public static $backendTypesSessionClassNames = [
        self::BACKEND_ADAPTER_SQL   => 'SQL',
        self::BACKEND_ADAPTER_FILES => 'Jig',
        self::BACKEND_ADAPTER_NOSQL => 'Mongo',
    ];

    /**
     * @return $this
     */
    public function initializePimpleContainer()
    {
        $pimpleContainer = new Pimple\Container();
//        Base::instance()->set(DI_CONTAINER, $pimpleContainer);
        Registry::set(DI_CONTAINER, $pimpleContainer);

        return $this;
    }

    /**
     * @return $this
     */
    public function initializeSqlAdapter()
    {
        $container                    = Registry::get(DI_CONTAINER);
        $container['storage_adapter'] = function ($c) {
            $f3       = \Base::instance();
            $settings = $f3->get('storage.sql');
            $dsn      = "mysql:host={$settings['host']};port={$settings['port']};dbname={$settings['dbname']}";
            $options  = null;

            if (isset($settings['persist']) && true === $settings['persist']) {
                $options[\PDO::ATTR_PERSISTENT] = true;
            }

            return new DB\SQL(
                $dsn,
                $settings['user'],
                $settings['password'],
                $options
            );
        };

        return $this;
    }

    /**
     * @return $this
     */
    public function initializeJigAdapter()
    {
        $container                    = Registry::get(DI_CONTAINER);
        $container['storage_adapter'] = function ($c) {
            $settings = \Base::instance()->get('storage.jig');

            return new DB\Jig($settings['dir']);
        };

        return $this;
    }

    /**
     * @param null $backendType
     * @return $this
     * @throws Exception
     */
    public function initializeBackendAdapter($backendType = null)
    {
        $f3 = Base::instance();
        if (is_null($backendType)) {
            $this->_guardAgainstUseStorageSettingDoesNotSet($backendType);
            $backendType = $f3->get(self::USE_STORAGE);
        }
        $this->_guardAgainstNotPermittedBackendType($backendType);

        $initialiseMethod = $f3->camelcase("initialize_{$backendType}_adapter");

        $this->_guardAgainstInitializingBackendMethodDoesNotExists($initialiseMethod);

        $container = Registry::get(DI_CONTAINER);

        $container['backend_type']          = self::$backendTypesSessionClassNames[$backendType];
        $container['mapper_class']          = 'DB\\' . $container['backend_type'] . '\\Mapper';
        $container['session_adapter_class'] = 'DB\\' . $container['backend_type'] . '\\Session';

        $this->$initialiseMethod();

        $container['mapper'] = $container->protect(function ($entity) use ($container) {
            return new $container['mapper_class']($container['storage_adapter'], $entity);
        });

        $container['mapper_proxy'] = $container->protect(function ($entity) use ($container) {
            return new MapperProxy($container['mapper']($entity));
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function initializeErrorHandler()
    {
        Base::instance()->set('ONERROR', 'Components\ErrorHandler::failureDispatch');

        return $this;
    }

    /**
     * @return $this
     */
    public function initializeSessionAdapter()
    {
        if ($sessionAdapterClass = Base::instance()->get('store_session_in_files')) {
            return $this->_initJigSessionAdapter();
        }

        $container                    = Registry::get(DI_CONTAINER);
        $container['session_adapter'] = function ($c) {
            return new $c['session_adapter_class']($c['storage_adapter']);
        };

        return $this;
    }

    /**
     * @return $this
     */
    public function initializeViewHelpers()
    {
        $f3        = Base::instance();
        $headTitle = new HeadTitle();
        $headMeta  = new HeadMeta();

        $headTitle->setSeparator(' - ');
        $headTitle->append($f3->get('head_title'));

        $headMeta->appendName('keywords', $f3->get('meta_keywords'));
        $headMeta->appendName('description', $f3->get('meta_description'));


        $f3->set('headTitleHelper', $headTitle);
        $f3->set('headMeta', $headMeta);
        $f3->set('menuHelper', MenuViewHelper::instance());

        return $this;
    }

    /**
     * @return $this
     */
    public function initializeExtAutoloader()
    {
        require PATH_LIBS . '/Ext/vendor/autoload.php';

        return $this;
    }

    /**
     * @throws Exception
     * @internal param $backendType
     * @return mixed
     */
    private function _guardAgainstUseStorageSettingDoesNotSet()
    {
        if (!Base::instance()->exists(self::USE_STORAGE)) {
            throw new Exception('The settings "use_storage" have not been red. Can not set backend adapter type. ');
        }
    }

    /**
     * @param $backendType
     * @throws Exception
     */
    private function _guardAgainstNotPermittedBackendType($backendType)
    {
        if (!array_key_exists($backendType, self::$backendTypesSessionClassNames)) {
            throw new Exception('Not permitted backend adapter type');
        }
    }

    /**
     * @param $initialiseMethod
     * @throws Exception
     */
    private function _guardAgainstInitializingBackendMethodDoesNotExists($initialiseMethod)
    {
        if (!method_exists($this, $initialiseMethod)) {
            throw new Exception('Cant initialise backend adapter');
        }
    }

    /**
     * @internal param $container
     * @return $this
     */
    private function _initJigSessionAdapter()
    {
        $container                    = Registry::get(DI_CONTAINER);
        $container['session_adapter'] = function ($c) {
            $jigAdapter = new DB\Jig(PATH_JIG);

            return new DB\Jig\Session($jigAdapter);
        };

        return $this;
    }
}

return Bootstrap::instance();
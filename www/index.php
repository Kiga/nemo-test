<?php
require_once '../application/configs/constants.php';
if (APPLICATION_ENV_PRODUCTION == APPLICATION_ENV) {
    error_reporting(0);
} else {
    error_reporting(-1);
}

//set_include_path(PATH_LIBS);
$includePathResult = set_include_path(get_include_path() . PATH_SEPARATOR . PATH_LIBS);
$environmentConfig = 'config.' . APPLICATION_ENV . '.ini';
if (!is_file(PATH_CONFIG . "/$environmentConfig")) {
    die($environmentConfig . ' have been not found, create it or copy&paste');
}
$f3 = require('fat3/base.php');
$f3->config(PATH_CONFIG . "/$environmentConfig");
$f3->config(PATH_CONFIG . '/routes.ini');
$f3->config(PATH_CONFIG . '/head.ini');



$bootstrap = require('bootstrap.php');
$bootstrap
    ->initializeErrorHandler()
    ->initializeExtAutoloader()
    ->initializePimpleContainer()
    ->initializeBackendAdapter()
    ->initializeSessionAdapter()
    ->initializeViewHelpers()
;

$f3->run();
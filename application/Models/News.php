<?php
namespace Models;

use Base;
use Bootstrap;
use Models\Useful\ModelEngine;
use MongoId;
use MongoRegex;

class News extends ModelEngine
{


    public static $fieldNamesCanBeUpdated = [
        'owner_id', 'header', 'body', 'updated_on'
    ];

    public static $standardInsertData = [
        'owner_id'   => 0,
        'header'     => 0,
        'body'       => 0,
        'created_on' => null,
        'updated_on' => null,
    ];

    protected $_name = 'news';

    public function __construct($newsId = null)
    {
        parent::__construct($newsId);
    }

    public function getPaginateForPublic($page, $length = 3)
    {
        $paginateData = $this->paginate($page - 1, $length);
//        foreach ($paginateData['subset'] as &$news) {
//            $news                   = $news->cast();
//            $news['date_of_create'] = date('d.m.y', $news['date_of_create']->sec);
//        }

        return $paginateData;
    }

    public function getPaginateForCp($start, $length, $postData)
    {
        $filter = [];
        if (!empty($postData['search']['value'])) {
            foreach (self::$conformity as &$row) {
                $filter['$or'][] = [$row => new MongoRegex('/.*?' . $postData['search']['value'] . '.*?/i')];
            }
        }

        foreach (self::$conformity as $key => &$row) {
            if (!empty($postData['columns'][$key]['search']['value'])) {
                $filter['$or'][] = [$row => new MongoRegex('/.*?' . $postData['columns'][$key]['search']['value'] . '.*?/i')];
            }
        }

        $options = [];
        if (isset($postData['order'][0]['column']) && isset($postData['order'][0]['dir'])) {
            $options = ['order' => [
                self::$conformity[$postData['order'][0]['column']] => self::$sort[$postData['order'][0]['dir']]]
            ];
        }
        $paginateData = $this->paginate($start, $length, $filter, $options);

        foreach ($paginateData['subset'] as &$news) {
            $news                   = $news->cast();
            $news['date_of_create'] = date('d.m.Y', $news['date_of_create']->sec);
            $news['action']         = json_encode($news['_id']);
//            echo '<pre>',print_r($news['date_of_create']->sec),'</pre>';exit;
        }
//        exit;
        $paginateData['recordsTotal']    = $paginateData['total'];
        $paginateData['recordsFiltered'] = $paginateData['total'];

        return $paginateData;
    }
}
<?php

namespace Models\Useful;

use Models\Exceptions\ModelException;

class ModelEngine extends MapperProxy
{

    /**
     * @var array
     */
    public static $fieldNamesCanBeUpdated = [];

    public static $standardInsertData = [];
    /**
     * @var
     */
    protected $_name;

    protected $mapper;

    /**
     * @param null $entityId
     */
    public function __construct($entityId = null)
    {
        $this->_guardAgainstCollectionNameDoesntSet();

        $container = \Registry::get(DI_CONTAINER);

        parent::__construct($container['mapper']($this->_name));

        if (!is_null($entityId)) {
            if (is_integer($entityId)) {
                $this->load(['@id => ?', $entityId]);
            } elseif (is_array($entityId) || is_string($entityId)) {
                $this->load($entityId);
            }
        }
    }

    /**
     * @param $entityFormData
     * @param bool $reset
     * @throws ModelException
     * @return array
     */
    public function addNewDocument($entityFormData, $reset = true)
    {
        $this->_guardAgainstInsertStandardIsEmpty();
        if (true === $reset) {
            $this->reset();
        }

        $this->safeHydrate($entityFormData);

        $insertResult = $this->insert();

        return $insertResult;
    }


    /**
     * @param $fieldName
     * @param $fieldValue
     * @return array
     * @throws ModelException
     */
    public function updateFieldData($fieldName, $fieldValue)
    {
        $this->_guardAgainstFieldnameCantBeUpdated($fieldName);

        $this->set($fieldName, $fieldValue);

        return $this->update();
    }

    /**
     * @param $updateData
     * @throws ModelException
     * @internal param $fieldName
     * @internal param $fieldValue
     * @return array
     */
    public function updateFieldListData(array $updateData)
    {
        $this->_guardAgainstIncorrectType($updateData);
        foreach ($updateData as $fieldName => $fieldValue) {
            $this->_guardAgainstFieldnameCantBeUpdated($fieldName);
            $this->set($fieldName, $fieldValue);
        }

        return $this->update();
    }

    /**
     * @param array $updateData
     * @param bool $checkForEmptiness
     * @throws \Models\Exceptions\ModelException
     * @return array
     */
    public function updateOnlyAuthorizedFields(array $updateData, $checkForEmptiness = false)
    {
        $this->_guardAgainstSelfMapperIsDry();
        $this->_guardAgainstIncorrectType($updateData);
        foreach ($updateData as $fieldName => $fieldValue) {
            if (!in_array($fieldName, static::$fieldNamesCanBeUpdated)
                || (true === $checkForEmptiness && empty($fieldValue))
            ) {
                continue;
            }

            $this->set($fieldName, $fieldValue);
        }

        return $this->update();
    }

    /**
     * @param $fieldName
     * @throws ModelException
     */
    protected function _guardAgainstFieldnameCantBeUpdated($fieldName)
    {
        if (!in_array($fieldName, static::$fieldNamesCanBeUpdated)) {
            throw new ModelException("Fieldnqame $fieldName can not be updated", ModelException::E_FIELDNAME_CANT_BE_UPDATED);
        }
    }

    /**
     * @throws ModelException
     */
    protected function _guardAgainstSelfMapperIsDry()
    {
        if ($this->dry()) {
            throw new ModelException('Current mapper cant not be dry for further update operation', ModelException::E_MAPPER_IS_DRY);
        }
    }

    /**
     * @param $updateData
     * @throws \Models\Exceptions\ModelException
     */
    private function _guardAgainstIncorrectType($updateData)
    {
        if (!is_array($updateData)) {
            throw new ModelException("Updating data is not an array", ModelException::E_UPDATING_DATA_INCORRECT_TYPE);
        }
    }

    /**
     * @throws \Models\Exceptions\ModelException
     */
    private function _guardAgainstCollectionNameDoesntSet()
    {
        if (!$this->_name) {
            throw new ModelException('Collection name can not be empty!', ModelException::E_COLLECTION_NAME_IS_EMPTY);
        }
    }

    /**
     * @throws \Models\Exceptions\ModelException
     */
    private function _guardAgainstInsertStandardIsEmpty()
    {
        if (empty(static::$standardInsertData)) {
            throw new ModelException('Standard of inserting data can not be empty!', ModelException::E_INSERT_STANDARD_IS_EMPTY);
        }
    }

    /**
     * @param $entityFormData
     * @param $insertData
     */
    public  function safeHydrate($entityFormData)
    {
        $insertData = [];
        foreach (static::$standardInsertData as $key => $val) {
            if (isset($entityFormData[$key])) {
                $insertData[$key] = $entityFormData[$key];
            }
        }

        foreach ($insertData as $key => $val) {
            $this->set($key, $val);
        }

        return $this;
    }

} 
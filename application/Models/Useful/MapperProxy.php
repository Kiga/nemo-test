<?php

namespace Models\Useful;

class MapperProxy
{

    protected $mapper;

    private $_methods = array();

    /**
     * @param \DB\Cursor $mapper
     * @internal param null $entityId
     */
    public function __construct(\DB\Cursor $mapper)
    {
        $this->mapper = $mapper;
        if ('SQL' === $mapper->dbtype()) {
            $this->decorateLoadMethod();
        }
    }

    public function __call($name, $args)
    {
        if (isset($this->_methods[$name])) {
            return call_user_func_array($this->_methods[$name], $args);

        } else if (is_callable([$this->mapper, $name])) {
            return call_user_func_array([$this->mapper, $name], $args);

        } else {
            throw new \RuntimeException("Method {$name} does not exist");
        }
    }

    private function decorateLoadMethod()
    {
        $lambda = function ($filter = null, array $options = null, $ttl = 0) {

            if (!is_null($filter) && isset($filter[0][0])) {
                $filter = $filter[0];
                $filter[0] = str_replace('@', '', $filter[0]);
            }

            return $this->mapper->load($filter, $options, $ttl);
        };

        $this->_methods['load'] = $lambda->bindTo($this, $this);
    }
} 
<?php
namespace Models\Useful;

use DB\Mongo;
use MongoDate;
use MongoTimestamp;

class StorageCallback
{

    public static function getCurrentTimestamp($value)
    {
        return date('Y-m-d H:i:s', time());
    }

    public static function getOwnerId($value)
    {
        return \Components\Auth::instance()->getCurrentUserId();
    }

    public static function setFieldToMongoTimestamp($value)
    {
        return (new MongoTimestamp());
    }

    public static function setFieldToMongoDate($value)
    {
        if ($value instanceof MongoDate) {
            return $value;
        }

        return (new MongoDate());
    }
} 
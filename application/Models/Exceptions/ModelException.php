<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02.07.2014
 * Time: 9:32
 */

namespace Models\Exceptions;

use \Exception;

class ModelException extends Exception
{
    const E_MAPPER_IS_DRY = 2;
    const E_FIELDNAME_CANT_BE_UPDATED = 3;
    const E_COLLECTION_NAME_IS_EMPTY = 4;
    const E_INSERT_STANDARD_IS_EMPTY = 5;
    const E_UPDATING_DATA_INCORRECT_TYPE = 21;


    public function __construct($exceptionMessage = '', $exceptionErrorCode = 0, $previouseException = null)
    {
        parent::__construct($exceptionMessage, $exceptionErrorCode, $previouseException);
    }
} 
<?php
namespace Helpers;

use Models\Organization;
use Prefab;


class OrganizationViewHelper extends Prefab
{
    public function renderStatusLabel($stateValue)
    {
        if (!array_key_exists($stateValue, Organization::$stateLabels)) {
            echo 'Не определён';

            return false;
        }

        echo Organization::$stateLabels[$stateValue];
    }
}
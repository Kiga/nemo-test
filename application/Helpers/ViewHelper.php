<?php
namespace Helpers;


class ViewHelper
{
    protected $_view;

    private static $allowedSuffixes = ['htm', 'html', 'tpl'];

    public function __construct()
    {
        $this->_view = \Template::instance();
        $this->server_name = \Base::instance()->get('SERVER.SERVER_NAME');
    }

    protected function _render($templateName, $templateSuffix = 'htm')
    {
        if (!$templateName) {
            return null; //@todo: error template here
        }

        $template = join('', array_slice(explode('\\', get_class($this)), -1)) . "/$templateName.$templateSuffix";

        echo $this->_view->render($template);
    }

    public function __set($name, $value)
    {
        if (!$name) {
            return;
        }
        \Base::instance()->set($name, $value);

        return null;
    }


} 
<?php
namespace Helpers;

use Base;

class CssHtmlHelper
{

    private $_hiveVariablePrefix;

    private $_validatorMessagesName = 'validatorMessages';

    public function __construct($hiveVariablePrefix, $validatorMessagesName = 'validatorMessages')
    {
        $this->_hiveVariablePrefix = $hiveVariablePrefix;

        $this->_validatorMessagesName = $validatorMessagesName;
    }

    public function getErrorStateIfExists($hiveVariableName, $stateClass = 'state-error', $defaultValue = '')
    {
        if (!$this->_composeHiveVariableAndCheck([$this->_validatorMessagesName, $hiveVariableName])) {
            return $defaultValue;
        }

        return $stateClass;
    }

    public function renderErrorTextBlock($hiveVariableName)
    {
        $f3                   = Base::instance();
        $compositeHiveVarName = $this->_composeHiveVariable([$this->_validatorMessagesName, $hiveVariableName]);
        if (!$f3->exists($compositeHiveVarName)) {
            return '';
        }

        echo '<em class="invalid">';
        foreach ($f3->get($compositeHiveVarName) as $reason => $message) {
            echo $message . '<br />';
        }
        echo '</em>';
    }

    public function valueOrDefault($hiveName, $defaultValue = '')
    {
        $f3                    = Base::instance();
        $compositeHiveVariable = $f3->get($this->_composeHiveVariable($hiveName));
        if (!$compositeHiveVariable) {
            return $defaultValue;
        }

        return $compositeHiveVariable;
    }

    public function imageValueOrHolderJs($imagesPrefix, $hiveName, $holderValue = ['w' => 100, 'h' => 100, 'text' => ''])
    {
        $f3                    = Base::instance();
        $compositeHiveVariable = $f3->get($this->_composeHiveVariable($hiveName));
        if (!$compositeHiveVariable) {
            $holderString = '';
            if (is_array($holderValue)) {
                $holderString = 'data-src="holder.js/' . $holderValue['w'] . 'x' . $holderValue['h'];
                if ($holderValue['text']) {
                    $holderString .= '/text:' . $holderValue['text'];
                }
                $holderString .= '"';
            } elseif (is_scalar($holderValue)) {
                $holderString = 'data-src="' . $holderValue . '"';
            }

            return $holderString;
        }

        $imageSrc = 'src="' . $imagesPrefix . $compositeHiveVariable . '"';

        return $imageSrc;
    }

    public function valueOrAnother($hiveVariableName, $anotherNameContainer)
    {
        $f3                   = Base::instance();
        $compositeHiveVarName = $this->_composeHiveVariable($hiveVariableName);
        $compositeAnother     = $anotherNameContainer . '.' . $hiveVariableName;
        if ($f3->exists($compositeHiveVarName)) {
            $hiveVarName = $f3->get($compositeHiveVarName);
            if (!empty($hiveVarName)) {
                return $hiveVarName;
            }

            return '';
        } else if ($f3->exists($compositeAnother)) {
            return $f3->get($compositeAnother);
        }

        return '';
    }

    public function valueFromStack($value, $keyValueStack)
    {
        if (!is_array($keyValueStack) || !array_key_exists($value, $keyValueStack)) {
            return '';
        }

        return $keyValueStack[$value];
    }

    public function setCheckedIfTrue($hiveVariableName)
    {
        return $this->getValueIfTrue($hiveVariableName, 'checked="checked"');
    }

    public function setSelectedIfValuesEqual($value1, $value2)
    {
        if ($value1 == $value2) {
            return 'selected="selected"';
        }

        return '';
    }

    public function setCheckedIfValuesEqual($value1, $value2)
    {
        return $this->_getValueIfParamsEqual($value1, $value2, 'checked="checked"');
    }

    private function _getValueIfParamsEqual($param1, $param2, $value = '', $default = '')
    {
        if ($param1 == $param2) {
            return $value;
        }

        return $default;
    }


    private function getValueIfTrue($hiveVariableName, $value = '')
    {
        $f3                    = Base::instance();
        $compositeHiveVariable = $f3->get($this->_composeHiveVariable($hiveVariableName));
        if (!$compositeHiveVariable) {
            return '';
        }

        return $value;
    }

    private function _composeHiveVariableAndCheck($appendPart)
    {
        return Base::instance()->exists($this->_composeHiveVariable($appendPart));
    }

    private function _composeHiveVariable($appendPart)
    {

        if (is_array($appendPart)) {
            $appendPart = implode('.', $appendPart);
        }

        return ($this->_hiveVariablePrefix . '.' . $appendPart);
    }
}
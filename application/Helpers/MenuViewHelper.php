<?php

namespace Helpers;

use \Base;
/**
 * Помогальник для всего, что связано с меню
 *
 * Class MenuViewHelper
 * @package Helpers
 */
class MenuViewHelper extends \Prefab
{
    const CSS_CLASS_ACTIVE = 'active';

    /**
     * @param $menuItem
     * @param string $paramsItem
     * @return string
     */
    public function getActiveClassIfMenuItemChosen($menuItem, $paramsItem = 'menuItem') {
        $menuArticle = Base::instance()->get("PARAMS.{$paramsItem}");
        if ( (is_array($menuItem) && in_array($menuArticle, $menuItem))
            || ($menuItem == $menuArticle)
        ) {
            return self::CSS_CLASS_ACTIVE;
        }

        return '';
    }

    /**
     * @param $menuItemValue
     * @param string $itemKey
     * @return string
     */
    public function getActiveClassIfChosen($menuItemValue, $itemKey = 'menuItem')
    {
        $menuArticle = Base::instance()->get($itemKey);
        if ( (is_array($menuItemValue) && in_array($menuArticle, $menuItemValue))
            || ($menuItemValue == $menuArticle)
        ) {
            return self::CSS_CLASS_ACTIVE;
        }

        return '';
    }
} 
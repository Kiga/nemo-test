<?php
/**
 * Класс помощник для вывода значений товара
 *
 * @author kigamuka
 */

namespace Helpers;


use Models\CurrencyRates;
use Models\Product;
use PhilipBrown\Money\Money;

class ProductViewHelper
{
    protected $currencies;

    public function __construct()
    {
        $this->currencies = (new CurrencyRates())->loadCurrencies();
    }

    public function printCostByObj(Product $product)
    {
        if (isset($product->cost_obj_rub)) {
            return $this->printCostByMoney($product->cost_obj_rub);
        } elseif (isset($product->cost_obj)) {
            return $this->printCostByMoney($product->cost_obj);
        } else if (isset($product->currency) && in_array($product->currency, CurrencyRates::$currencies)) {
            $cost = isset($product->cost) ? $product->cost : 0;

            return $this->printCostByMoney(Money::init($product->cost, $this->currency));
        }

    }

    public function printCostByMoney(Money $money)
    {
        $cost = bcdiv($money->cents, 100);

        echo $cost . ' ' . $money->currency->getHtmlEntity();
    }

    public function prCost($product)
    {

    }

}
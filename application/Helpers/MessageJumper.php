<?php
namespace Helpers;

use Base;


/**
 * Помогальник.
 * Сейвит данные с мессагами на 1 прыжок для дальнейшего отображения пользователю
 *
 * Class MessageJumper
 * @package Helpers
 */
class MessageJumper extends \Prefab
{
    const SUCCESS = 1;

    const LOGIN_SUCCESS                   = 1;
    const LOGIN_ERROR_LP_INCORRECT        = -18;
    const LOGIN_ERROR_CANT_LOAD_USER_DATA = -19;

    const RESULT_SUCCESS = 'success';
    const RESULT_ERROR   = 'warning';
    const RESULT_FAILURE = 'danger';

    const CONTAINER_NAME = 'SESSION.messageJumperData';
    const PRELOGED_CONTAINER_NAME = 'SESSION.prelogedRequestData';

    public function __construct()
    {
    }

    /**
     * Сохранить данные с сообщениями и прыгнуть по маршруту
     *
     * @param null $route
     * @param $data
     * @param string $messages
     * @param string $type
     * @return mixed
     */
    public function saveDataAndJump($route = null, $data, $messages = '', $type = self::RESULT_SUCCESS)
    {
        $f3               = Base::instance();
        $sessionContainer = ['hopes' => 1, 'data' => $data, 'resultMessages' => $messages, 'type' => $type];
        $f3->set(self::CONTAINER_NAME, $sessionContainer);

        return $f3->reroute($route);
    }

    /**
     * @param $route
     * @param $messages
     * @param $type
     * @return mixed
     */
    public function saveMessageAndJump($route, $messages, $type)
    {
        $f3               = Base::instance();
        $sessionContainer = ['hopes' => 1, 'resultMessages' => $messages, 'type' => $type];
        $f3->set(self::CONTAINER_NAME, $sessionContainer);

        return $f3->reroute($route);
    }

    /**
     * @param null $containerKey
     * @param string $containerName
     * @return null
     */
    public function getData($containerKey = null, $containerName = self::CONTAINER_NAME)
    {
        $f3 = Base::instance();
        if (!$f3->exists($containerName)) {
            return null;
        }

        $container = $f3->get($containerName);
        if (!--$container['hopes']) {
            $f3->clear($containerName);
        } elseif (!empty($container)) {
            $f3->set($containerName, $container);
        }

        $this->transitMessagesToView($container);

        if (!$containerKey) {
            return $container;
        } else if (!array_key_exists($containerKey, $container)) {
            return null;
        }


        return $container[$containerKey];
    }

    /**
     * @param $container
     * @return mixed
     */
    private function transitMessagesToView($container)
    {
        $f3 = Base::instance();
        if (isset($container['resultMessages'])) {
            $f3->set('flash.messages', $container['resultMessages']);
        }

        if (isset($container['type'])) {
            $f3->set('flash.type', $container['type']);
        }
    }

    /**
     * @param $code
     * @param $message
     * @return array
     */
    public function prepareResult($code, $message)
    {
        return ['code' => $code, 'data' => $message];
    }
} 
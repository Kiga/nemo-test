<?php
/**
 * Класс помощник для работы с GET-шлаком
 *
 * @author kigamuka
 */

namespace Helpers;

use Purl\Query;

class QueryHelper
{
    private $_query;

    public function __construct()
    {
        $this->_query = new Query();
    }

    /**
     * @return Query
     */
    public function getQ()
    {
        return $this->_query;
    }

    /**
     * @param Query $query
     */
    public function setQ($query)
    {
        $this->_query = $query;
    }

    public function setData($data)
    {
        $this->getQ()->setData($data);

        return $this;
    }

    public function cloneAndSet($name, $value)
    {
        $url = new Query($this->getQ()->getQuery());
        $url->set($name, $value);

        return $url;
    }

    public function get($key)
    {
        return $this->getQ()->get($key);
    }

    /**
     * @inheritDoc
     */
    public function __toString()
    {
        return $this->getQ()->getQuery();
    }
}
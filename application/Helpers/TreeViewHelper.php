<?php
namespace Helpers;

use Models\Section;
use Models\Useful\INestedSets;
use Prefab;

/**
 * Помогальник для отображения деревьев
 *
 * Class TreeViewHelper
 * @package Helpers
 */
class TreeViewHelper extends Prefab
{
    public static function render()
    {
        // концепция в разработке
    }

    /**
     * @param INestedSets $nodeMappedList
     * @param string $parentId
     */
    public function getNodesFromListByParentId(INestedSets $nodeMappedList, $parentId = '0')
    {
        while (!$nodeMappedList->dry()) { // gets dry when we passed the last record
            if ($nodeMappedList->parent_id == $parentId) {
                yield $nodeMappedList->cast(); // it's ЖАТВА time
            }

            // moves forward even when the internal pointer is on last record
            $nodeMappedList->next();
        }
    }


    public function renderTreeForPublic()
    {
        $nodeMappedList = Section::getVisibleNodesCursor();
        $preOl          = [];
        while (!$nodeMappedList->dry()) {
            $diff        = $nodeMappedList->right - $nodeMappedList->left;
            $classClause = ($diff > 1) ? " class='dropdown-submenu'" : '';
            $sectionIdOrUrl = mb_strlen($nodeMappedList->friendly_url) ? $nodeMappedList->friendly_url : $nodeMappedList->name;

            echo "<li{$classClause} data-id='{$nodeMappedList->_id}' data-parent_id='{$nodeMappedList->parent_id}'>" .
                "<a href='/catalog/{$sectionIdOrUrl}'>{$nodeMappedList->name}</a>";
            if ($diff > 1) {
                $preOl[] = $nodeMappedList->right - 1;
                echo "<ul class='dropdown-menu'>";
            }

            if (($nodeMappedList->right == end($preOl))) {
                echo "</ul></li>";
                array_pop($preOl);
            } elseif ($diff == 1) {
                echo '</li>';
            }
            $nodeMappedList->next();
        }
    }

    public function renderTreeForCp(INestedSets $nodeMappedList)
    {
        $preOl = [];
        while (!$nodeMappedList->dry()) {
            echo '<li class="dd-item dd3-item" data-id="' . $nodeMappedList->_id . '" data-parent_id="' . $nodeMappedList->parent_id . '">';
            echo '<div class="dd-handle dd3-handle">Drag</div>';
            echo '<div class="dd3-content cursor">' . $nodeMappedList->name . '</div>';
            $diff = $nodeMappedList->right - $nodeMappedList->left;
            if ($diff > 1) {
                $preOl[] = $nodeMappedList->right - 1;
                echo '<ol class="dd-list">';
            }

            if (($nodeMappedList->right == end($preOl))) {
                echo '</ol></li>';
                array_pop($preOl);
            } elseif ($diff == 1) {
                echo '</li>';
            }
            $nodeMappedList->next();
        }
    }

    public function renderTreeForJs()
    {
        $section = new Section();
        $section->load(['left' => ['$gt' => 0]], ['order' => ['level' => 1, 'left' => 1]]);

        echo "<script type=\"text/javascript\">\nvar nodeList = [\n";
        while (!$section->dry()) {
            echo '{"id": "', $section->_id, '", "parent_id": "', $section->parent_id, '", "text": "', $section->name, '", "level": "', $section->level, '"}';

            $section->next();
            if (!$section->dry()) {
                echo ",\n";
            }
        }
        echo "\n];\n</script>";
    }
}
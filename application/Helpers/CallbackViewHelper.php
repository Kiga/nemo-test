<?php
namespace Helpers;

use Models\Callback;
use Prefab;


class CallbackViewHelper extends Prefab
{
    public function renderStatusLabel($stateValue)
    {
        if (!array_key_exists($stateValue, Callback::$stateLabels)) {
            echo 'Не определён';

            return false;
        }

        echo Callback::$stateLabels[$stateValue];
    }
}
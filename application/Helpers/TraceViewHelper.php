<?php
namespace Helpers;
/**
 * Готовит трассировку вызовов для рендера
 *
 * Class TraceViewHelper
 * @package Helpers
 */
class TraceViewHelper
{
    public static function prepareTrace()
    {
        $f3 = \Base::instance();
        $traceData = $f3->get('ERROR.trace');
        $traceText = '';
        $iterator  = 0;
        foreach ($traceData as $frame) {
            $line = '';
            if ($f3->get('DEBUG') > 1) {
                if (isset($frame['class'])) {
                    $line .= $frame['class'] . $frame['type'];
                }
                if (isset($frame['function'])) {
                    $line .= $frame['function'] . '(';
                    if (isset($frame['args'])) {
                        $line .= '<button type="button" class="btn btn-info btn-xs args-button" data-target="#args_' . $iterator . '">args</button><div class="alert-info args-box hidden" id="args_' . $iterator . '">' . print_r($frame['args'], true) . '</div>';
                    }
                    $line .= ')';
                }
            }
            $src = $f3->fixslashes(str_replace($_SERVER['DOCUMENT_ROOT'] . '/', '', $frame['file'])) . ':' . $frame['line'] . ' ';
            $traceText .= '#' . $iterator++ . ' ' . $src . $line . "\n";
        }

        return $traceText;
    }



}
<?php
namespace Helpers;

use stdClass;

/**
 * Помогальник.
 * (((
 *
 * Class ShowJson
 * @package Helpers
 */
class ShowJson extends \Prefab
{
    /**
     *
     */
    const RESULT_ERROR = -1;
    /**
     *
     */
    const RESULT_SUCCESS = 1;
    /**
     *
     */
    const RESULT_FAILURE = 2;
    /**
     *
     */
    const RESULT_WARNING = 3;

    /**
     * @var \Base
     */
    private $_f3;

    /**
     *
     */
    public function __construct()
    {
        $this->_f3 = \Base::instance();
    }

    /**
     * Передать данные в джейсоне
     *
     * @param $code
     * @param $data
     * @param null $extra
     * @return mixed
     */
    public function answer($code, $data, $extra = null)
    {
        header('Content-Type: application/json');

        $response        = new stdClass();
        $response->code  = $code;
        $response->data  = $data;
        $response->extra = $extra;

        echo json_encode($response);
        exit;
    }

    /**
     * @param array $answerData
     */
    public function answerViaArray(array $answerData)
    {
        header('Content-Type: application/json');

        echo json_encode($answerData);
        exit;
    }

    /**
     * @param array $errorMessages
     * @param int $code
     */
    public function answerWithError(array $errorMessages, $code = self::RESULT_FAILURE)
    {
        header('Content-Type: application/json');

        $response                = new stdClass();
        $response->code          = $code;
        $response->error         = 1;
        $response->error_details = $errorMessages;

        echo json_encode($response);
        exit;
    }

    /**
     * @param $code
     */
    public function answerByCode($code)
    {
        header('Content-Type: application/json');

        echo json_encode(["code" => $code]);
        exit;
    }
} 
<?php

function getThumb($image, $size, $options = []) {
    $image = PATH_WWW . $image;
    if (!file_exists($image)) {
        return false;
    }
    return \Bazalt\Thumbs\Image::getThumb($image, $size, $options);
}

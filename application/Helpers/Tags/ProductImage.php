<?php
namespace Helpers\Tags;

use Base;
use Prefab;
use Template;

class ProductImage extends Prefab
{

    /**
     * init template extension
     */
    public function __construct()
    {
        Template::instance()->extend('pim', 'ProductImage::render');
    }

    /**
     * link parser template helper
     * @param $args
     * @internal param array $node
     * @return string
     */
    public static function render($args)
    {
        // retrieve the attributes of the template tag, as found in the template
        // in our case, we expect 'src', 'width' and 'height', and optionally 'crop'
        $attr = $args['@attrib']; // provided by the F3 template engine
        // retrieve the inner html, as found in the template
        $html = (isset($args[0])) ? $args[0] : '';

        $imagepath = $attr['src'];
        $imgObj    = new \Image($imagepath);
        $imgObj->resize(
            $attr['width'],
            $attr['height'],
            ((isset($attr['crop']) && $attr['crop'] == 'true') ? true : false)
        );
        $f3 = Base::instance();
        // to avoid clash, build a unique name for the new generated image
        $file_name = $f3->hash($imagepath . $attr['width'] . $attr['height']) . '.png';
        // save it for example in TEMP
        $imagepath = $f3->get('TEMP') . $file_name;
        // convert it to PNG and save it to a file
        $f3->write($imagepath, $imgObj->dump('png'));

        // done! return the HTML markup
        return
            sprintf('<img src="/%s" width="%u" height="%u" />',
                $imagepath, $attr['width'], $attr['height']);
    }
}
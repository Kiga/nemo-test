<?php
/**
 * Класс помощник для вывода разницы во времени между входным параметром и текущим временем
 *
 * @author Nesterko
 */

namespace Helpers;


class ElapsedtimeViewHelper
{

    /**
     * Возвращает разницу во времени в человекотибельном формате (1 год, 2 месяца, 6 дней) между входным и текущим временем
     *
     * @param datetime $timeStamp - время в формате Y-m-d H:i:s, хотя всё равно будет преобразовано через strtotime
     * @param array $gettingParts - массив кусков, которые нужны: array('months')|array('years', 'months'), будут показаны только эти куски, остальные отброшены
     * @return string - строка вида: 1 год, 2 месяца, 23 дня
     */
    public function elapsedTime($timeStamp, $gettingParts = null, $fullset = false)
    {

        $result = strtotime($timeStamp);
        if (false === $result) {
            return 'очень давно';
        }

        $diff = time() - $result;
        if ($diff < 86400 && !$fullset) {
            return 'менее суток';
        }
        $gettingParts = (array) $gettingParts;

        $years   = floor($diff / (365 * 60 * 60 * 24));
        $months  = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days    = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24)/ (60 * 60 * 24));
        $hours   = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24)/ (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60)/ (60));
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));

        $result = array();
        if ($years > 0) {
            $result['years']   = $years . ' ' . $this->_plural($years, 'год', 'года', 'лет');
        }
        if ($months > 0) {
            $result['months']  = $months . ' ' . $this->_plural($months, 'месяц', 'месяца', 'месяцев');
        }
        if ($days > 0) {
            $result['days']    = $days . ' ' . $this->_plural($days, 'день', 'дня', 'дней');
        }
        if ($fullset) {
            if ($hours > 0) {
                $result['hours']   = $hours . ' ' . $this->_plural($hours, 'час', 'часа', 'часов');
            }
            if ($minutes > 0) {
                $result['minutes'] = $minutes . ' ' . $this->_plural($minutes, 'минута', 'минуты', 'минут');
            }
            if ($seconds > 0) {
                $result['seconds'] = $seconds . ' ' . $this->_plural($seconds, 'секунда', 'секунды', 'секунд');
            }
        }

        if (!empty($gettingParts)) {
            $gettingParts = array_combine($gettingParts, array_fill(0, count($gettingParts), 0));
            $result       = array_intersect_key($result, $gettingParts);
        }

        return implode(', ', $result);
    }

    /**
     * Возвращает множественную форму по числу
     *
     * @param int $count    - число по которому берется множественная форма
     * @param string $form1 - первая форма: 1 ден
     * @param string $form2 - вторая форма: 2 дня
     * @param string $form3 - третья форма: 6 дней
     * @return string - множественная форма для числа
     */
    private function _plural($count, $form1, $form2, $form3)
    {
        $count = str_replace(' ', '', $count);
        if ($count > 10 && floor(($count % 100) / 10) == 1) {
            return $form3;
        } else {
            switch ($count % 10) {
                case 1: return $form1;
                case 2:
                case 3:
                case 4: return $form2;
                default: return $form3;
            }
        }
    }
}
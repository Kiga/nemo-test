<?php

// Application
define('APPLICATION_ENV',      getenv('APPLICATION_ENV') ?: 'production');
define('APPLICATION_ENCODING', 'utf-8');
define('APPLICATION_ENV_PRODUCTION', 'production');

// Paths
define('PATH_BASE',         str_replace('\\', '/', realpath(__DIR__ . '/../..')));
define('PATH_APPLICATION',  PATH_BASE . '/application/');
define('PATH_MODULES',  PATH_BASE . '/application/modules');
define('PATH_LIBS',         PATH_BASE . '/library');
define('PATH_DATA',          PATH_BASE . '/application/data');
define('PATH_STORAGE',          PATH_BASE . '/storage');
define('PATH_JIG',          PATH_STORAGE . '/jig/');
define('PATH_WWW',          PATH_BASE . '/www');

define('PATH_CONFIG',       PATH_APPLICATION . '/configs');
define('PATH_TEMPLATES',    PATH_APPLICATION . '/modules/');
define('PATH_TMP',          PATH_APPLICATION . '/data/tmp/');

define('PATH_TEMPLATES_COMPONENTS', PATH_APPLICATION . '/Components/views/');

define('DI_CONTAINER', 'di_container');
<?php
namespace Components;

use Base;
use Components\Auth;
use Helpers\MenuViewHelper;
use Template;

class PublicController
{
    protected $_layout = null;

    /**
     * @var Template
     */
    protected $_view;

    protected $_alreadyRendered;

    protected $_viewTtl = 0;

    public function __construct()
    {
        $f3          = Base::instance();
        $this->_view = Template::instance();

        $f3->set('UI', $f3->get('UI') . $this->_getModuleFolder() . '/views/');
        $this->_alreadyRendered = false;
    }

    public function hyphenRouteToCamelCaseRoute($f3, $params)
    {
        //@todo: think about it in midload terms
        $action     = $f3->get('PARAMS.action');
        $methodName = $f3->camelcase(implode('_', explode('-', $action)));

        $this->_guardAgainstMethodNotExists($methodName);

        return $this->$methodName($f3, $params);
    }

    public function __set($name, $value)
    {
        if (!$name) {
            return;
        }
        Base::instance()->set($name, $value);

        return null;
    }

    /**
     * Event post-route hanlder
     *
     * @param $f3
     * @param $params
     */
    public function afterroute($f3, $params)
    {
        if ($f3->exists('content')
            && !empty($this->_layout)
            && false == $this->_alreadyRendered
        ) {
            echo $this->_view->render($this->_layout, 'text/html', null, $this->_viewTtl);
        }
    }

    /**
     * Event pre-route handler.
     *
     * @param \Base $f3
     * @param $params
     */
    public function beforeroute($f3, $params)
    {
        $action = $f3->exists('PARAMS.action') ? $f3->get('PARAMS.action') : 'index';

        $this->_setLayoutData($f3, ['controller' => $this->_getControllerShortName(), 'action' => $action]);
    }

    protected function _setLayout($layoutName)
    {
        $this->_guardAgainstLayoutNameIsEmpty($layoutName);
        $this->_layout = "../layouts/{$layoutName}";
    }

    protected function _render($templateName, $templateSuffix = 'htm')
    {
        $this->_guardAgainstTemplateNameIsEmpty($templateName);

        $controllerFolder = $this->_getControllerFolder();
        $template         = "/$controllerFolder/$templateName.$templateSuffix";

        $this->_alreadyRendered = true;
        echo $this->_view->render($template);

        return true;
    }

    protected function _renderWithLayout($templateName, $templateSuffix = 'htm')
    {
        $this->_willBeRenderedWithLayout($templateName, $templateSuffix);

        $this->_alreadyRendered = true;
        echo $this->_view->render($this->_layout);

        return true;
    }

    protected function _willBeRenderedWithLayout($templateName, $templateSuffix = 'htm', $ttl = 0)
    {
        $f3 = Base::instance();
        $this->_guardAgainstTemplateNameIsEmpty($templateName);
        $this->_guardAgainstLayoutIsEmpty();

        $controllerFolder = $this->_getControllerFolder();
        $f3->set('content', "/{$controllerFolder}/$templateName.$templateSuffix");

        if (is_file(PATH_MODULES . "/{$this->_getModuleFolder()}/views/$controllerFolder/$templateName-js.$templateSuffix")) {
            $f3->set('js_content', "/{$controllerFolder}/$templateName-js.$templateSuffix");
        }

        if ($ttl > 0) {
            $this->_viewTtl = $ttl;
        }

        return true;
    }

    /**
     * @return string
     */
    protected function _getModuleFolder()
    {
        $classNameParts = explode('\\', get_called_class());

        return ucfirst(mb_strtolower($classNameParts[0]));
    }

    /**
     * @return string
     */
    protected function _getControllerFolder()
    {
        $classNameParts   = explode('\\', get_class($this));
        $controllerFolder = mb_strtolower(mb_substr(join('', array_slice($classNameParts, -1)), 0, -10));

        return $controllerFolder;
    }

    /**
     * @param $methodName
     */
    protected function _guardAgainstMethodNotExists($methodName)
    {
        if (!method_exists($this, $methodName)) {
            Base::instance()->error(404);
        }
    }

    /**
     * @param $layoutName
     * @throws \Exception
     */
    protected function _guardAgainstLayoutNameIsEmpty($layoutName)
    {
        if (!$layoutName) {
            throw new \Exception('Layout name can not be empty');
        }
    }

    /**
     * @param $templateName
     * @throws \Exception
     */
    protected function _guardAgainstTemplateNameIsEmpty($templateName)
    {
        if (!$templateName) {
            throw new \Exception('Template name can not be empty');
        }
    }

    /**
     * @throws \Exception
     */
    protected function _guardAgainstLayoutIsEmpty()
    {
        if (!$this->_layout) {
            throw new \Exception('The layout template can not be empty. Please set it before rendering.');
        }
    }

    /**
     * @param \Base $f3
     * @param array $data
     */
    private function _setLayoutData($f3, $data = [])
    {
//        $f3->set('loginedUserData', User::getLoginedUserData());
        $f3->mset($data);
        $f3->set('menuItem', ($data['controller'] . '.' . $data['action']));
        $f3->set('menuHelper', MenuViewHelper::instance());

        $controller = isset($data['controller']) ? $data['controller'] : '';
        $action     = isset($data['action']) ? $data['action'] : '';

        $f3->set('controller', $controller);
        $f3->set('action', $action);
    }


    /**
     * @return string
     */
    private function _getControllerShortName()
    {
        return $this->_getControllerFolder();
    }
}
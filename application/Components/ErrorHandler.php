<?php

namespace Components;

use Base;
use Helpers\TraceViewHelper;
use Template;

/**
 * Обработчик и диспетчер ошибок
 *
 * Class ErrorHandler
 * @package Components
 */
class ErrorHandler
{
    /**
     * @param \Base $f3 Base
     * @return bool
     */
    public static function failureDispatch($f3)
    {
        static $step = 0;
        ++$step;
        $f3->set('UI', PATH_TEMPLATES_COMPONENTS);

//        $template         = Template::instance();
        $templateFileName = 'error.' . APPLICATION_ENV . '.htm';
        $templatePath     = PATH_APPLICATION . '/Components/views/' . $templateFileName;
        if (!is_file($templatePath)) {
//            ob_clean();
            return self::dispatchInvalidActionPage($f3);
        }

        if ($f3->get('AJAX')) {
            self::showAjaxError($f3, true);

            return true;
        }
//        $templateBody = file_get_contents($templatePath);
//        echo $template->resolve($templateBody, ['ERROR' => $f3->get('ERROR'), 'trace' => TraceViewHelper::prepareTrace()]);


        try {
            if (2 === ++$step) {
                $f3->set('trace', '');
            } else {
                $f3->set('trace', TraceViewHelper::prepareTrace());
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            //throw new baseException('', 0, $e);
        }
//        ob_clean();
        echo Template::instance()->render($templateFileName);

        return true;
    }

    /**
     * @param \Base $f3
     * @return bool
     */
    public static function dispatchProductionError($f3)
    {
        if ($f3->get('AJAX')) {
            self::showAjaxError($f3);

            return true;
        }
        $f3->set('UI', PATH_TEMPLATES_COMPONENTS);
        $templateFileName = 'error.' . APPLICATION_ENV_PRODUCTION . '.htm';

//        ob_clean();
        echo Template::instance()->render($templateFileName);

        return true;
    }

    /**
     * @param \Base $f3
     */
    public static function dispatchInvalidActionPage($f3)
    {
        $f3->status(400);
        if ($f3->get('AJAX')) {
            header('Content-Type: application/json');
            echo json_encode(['error' => 1]);
            exit;
        }
        $templatePath = PATH_APPLICATION . '/Components/views/error.invalid-action.htm';
        $templateBody = file_get_contents($templatePath);

        echo Template::instance()->resolve($templateBody);
        exit;
    }

    /**
     * @param \Base $f3
     * @param null $withHtmlTemplate
     * @internal param $extraData
     */
    private static function showAjaxError($f3, $withHtmlTemplate = null)
    {
        $error          = $f3->get('ERROR');
        $error['error'] = 1;
        $extraData      = null;
        if ($f3->exists('exception_extra', $extraData)) {
            $error['exception_extra'] = $extraData;
        }

        if (!is_null($withHtmlTemplate)) {
//            $templateFileName = 'error.' . APPLICATION_ENV . 'ajax.htm';
            $templateFileName = 'error.ajax.htm';
            $f3->set('trace', TraceViewHelper::prepareTrace());
            $error['error_html_template'] = Template::instance()->render($templateFileName);
        }

        ob_clean();
        header('Content-Type: application/json');
        echo json_encode($error);
    }

//    public static function syncErrorDispatch()
//    {
//
//    }
//
//    public static function asyncErrorDispatch()
//    {
//
//    }
}

//100: ('Continue', 'Request received, please continue'),
//101: ('Switching Protocols', 'Switching to new protocol; obey Upgrade header'),
//
//200: ('OK', 'Request fulfilled, document follows'),
//201: ('Created', 'Document created, URL follows'),
//202: ('Accepted','Request accepted, processing continues off-line'),
//203: ('Non-Authoritative Information', 'Request fulfilled from cache'),
//204: ('No Content', 'Request fulfilled, nothing follows'),
//205: ('Reset Content', 'Clear input form for further input.'),
//206: ('Partial Content', 'Partial content follows.'),
//
//300: ('Multiple Choices', 'Object has several resources -- see URI list'),
//301: ('Moved Permanently', 'Object moved permanently -- see URI list'),
//302: ('Found', 'Object moved temporarily -- see URI list'),
//303: ('See Other', 'Object moved -- see Method and URL list'),
//304: ('Not Modified', 'Document has not changed since given time'),
//305: ('Use Proxy', 'You must use proxy specified in Location to access this ''resource.'),
//307: ('Temporary Redirect', 'Object moved temporarily -- see URI list'),
//
//400: ('Bad Request', 'Bad request syntax or unsupported method'),
//401: ('Unauthorized', 'No permission -- see authorization schemes'),
//402: ('Payment Required', 'No payment -- see charging schemes'),
//403: ('Forbidden', 'Request forbidden -- authorization will not help'),
//404: ('Not Found', 'Nothing matches the given URI'),
//405: ('Method Not Allowed', 'Specified method is invalid for this server.'),
//406: ('Not Acceptable', 'URI not available in preferred format.'),
//407: ('Proxy Authentication Required', 'You must authenticate with this proxy before proceeding.'),
//408: ('Request Timeout', 'Request timed out; try again later.'),
//409: ('Conflict', 'Request conflict.'),
//410: ('Gone', 'URI no longer exists and has been permanently removed.'),
//411: ('Length Required', 'Client must specify Content-Length.'),
//412: ('Precondition Failed', 'Precondition in headers is False.'),
//413: ('Request Entity Too Large', 'Entity is too large.'),
//414: ('Request-URI Too Long', 'URI is too long.'),
//415: ('Unsupported Media Type', 'Entity body in unsupported format.'),
//416: ('Requested Range Not Satisfiable', 'Cannot satisfy request range.'),
//417: ('Expectation Failed', 'Expect condition could not be satisfied.'),
//
//500: ('Internal Server Error', 'Server got itself in trouble'),
//501: ('Not Implemented','Server does not support this operation'),
//502: ('Bad Gateway', 'Invalid responses from another server/proxy.'),
//503: ('Service Unavailable','The server cannot process the request due to a high load'),
//504: ('Gateway Timeout', 'The gateway server did not receive a timely response'),
//505: ('HTTP Version Not Supported', 'Cannot fulfill request.'),
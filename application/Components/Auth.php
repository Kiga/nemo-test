<?php
namespace Components;

use Audit;
use Base;
use Bootstrap;
use DB\Mongo\Mapper;
use MongoDBRef;
use Helpers\MessageJumper;

class Auth extends \Prefab
{
    private $_auth;

    private $_audit;

    private $container;

    public function __construct()
    {
        $this->container = \Registry::get(DI_CONTAINER);

//        $userMapper   = $this->container['mapper']('users');
        $userMapper   = new $this->container['mapper_class']($this->container['storage_adapter'], 'users');
        $this->_auth  = new \Auth($userMapper, ['id' => 'email', 'pw' => 'password']);
        $this->_audit = Audit::instance();
    }

    /**
     * Получить пользовательские данные из сессии
     * Если пользователь не залогинен, вернет false
     * @return boolean|mixed
     */
    public function getUserData()
    {
        if (!$this->hasUserLogined()) {
            return false;
        }

        return Base::instance()->get('SESSION.user');
    }

    public function getCurrentUserId()
    {
        if ($userData = $this->getUserData()) {
            return $userData['id'];
        }

        return false;
    }

    public function getCurrentUserRepresentationId()
    {
        if ($userId = $this->getCurrentUserId()) {
            return $userId->__toString();
        }

        return false;
    }

    public function hasUserLogined()
    {
        return Base::instance()->exists('SESSION.user');
    }

    public function login($login, $password)
    {
        if (!$this->checkCredentials($login, $password)) {
            return false;
        }
        $userMapper = $this->container['mapper_proxy']('users');
        $filter     = ['@email = ? AND @password = ?', $login, $this->getPasswordProcessing($password)];
        $userData   = $userMapper->load($filter);
        if (!$userData) {
            return false;
        }

        $this->restoreUserData($userData);

        return true;
    }

    public function checkCredentials($login, $password)
    {
        $passwordProcessing = $this->getPasswordProcessing($password);

        return $this->_auth->login($login, $passwordProcessing);
    }

    /**
     * @param $password
     * @return mixed
     */
    public function getPasswordProcessing($password)
    {
        $f3                 = Base::instance();
        $salt               = $f3->get('auth.salt');
        $passwordAlgorithm  = $f3->get('auth.algo.func');
        $passwordArgs       = $f3->get('auth.algo.args', [$salt, $password]);
        $passwordProcessing = $f3->call($passwordAlgorithm, $passwordArgs);

        return $passwordProcessing;
    }

    public function logout()
    {
        return Base::instance()->clear('SESSION');
    }

    /**
     * @param User $userData
     * @return NULL
     */
    public function restoreUserData($userData)
    {
        return $userData->copyto('SESSION.user');
    }

    /**
     * Clear all session data except user preloged data
     */
    public function clearSessionExeptsPrelogedData()
    {
        $f3       = Base::instance();
        $memoData = $f3->get(MessageJumper::PRELOGED_CONTAINER_NAME);
        $f3->clear('SESSION');
        if (!empty($memoData)) {
            $f3->set(MessageJumper::PRELOGED_CONTAINER_NAME, $memoData);
        }

    }
}
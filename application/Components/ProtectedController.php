<?php
namespace Components;

use Base;
use Helpers\MenuViewHelper;
use Helpers\MessageJumper;
use Models\Acl\Access;
use Models\User;
use Template;

/**
 * Class ProtectedController
 * @package Components
 */
class ProtectedController extends PublicController
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Event pre-route handler.
     *
     * @param \Base $f3
     * @param $params
     */
    public function beforeroute($f3, $params)
    {
        $availablePatters = ['/auth', '/auth/login', '/auth/logout', '/lockscreen', '/auth/get-pass-hash/@check'];

        if (!in_array($f3->get('PATTERN'), $availablePatters) &&
            false == Auth::instance()->hasUserLogined()
        ) {
            $this->_rememberUserRequest($f3);

            return $f3->reroute('@login_form');
        }

        parent::beforeroute($f3, $params);
    }

    /**
     * @param \Base $f3
     */
    private function _rememberUserRequest($f3)
    {
//        $isAjax   = $f3->get('AJAX');
        $prelogedContainer = [
            'hopes'    => 4,
            'uri'      => $f3->get('URI'),
            'postData' => $f3->get('POST')
        ];

        $f3->set(MessageJumper::PRELOGED_CONTAINER_NAME, $prelogedContainer);
    }
}
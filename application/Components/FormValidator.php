<?php

namespace Components;

use Zend\InputFilter\Factory;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\ValidatorInterface;

class FormValidator
{
    private $_inputFilter;

    /**
     * @param mixed $filter
     */
    public function setFilter($filter)
    {
        $this->_inputFilter = $filter;
    }

    /**
     * @return InputFilterInterface
     */
    public function getFilter()
    {
        return $this->_inputFilter;
    }


    public function __construct($validationConfig, $validationConfigSuffix = 'ini')
    {
        $validationConfigPath = PATH_CONFIG .  "/forms/{$validationConfig}.{$validationConfigSuffix}";
        $this->_guardAgainstConfigNotExists($validationConfigPath);

        $f3 = \Base::instance();
        $f3->clear('inputFilters');
        $f3->config($validationConfigPath);

        $this->_guardAgainstFiltersOptionsNotFound($f3);

        $inputFiltersOptions = $f3->get('inputFilters');

        $this->_inputFilter = (new Factory())->createInputFilter($inputFiltersOptions);
    }

    /**
     * @param $validationConfigPath
     * @throws \Exception
     */
    private function _guardAgainstConfigNotExists($validationConfigPath)
    {
        if (!is_file($validationConfigPath)) {
            throw new \Exception('Validation config file not found');
        }
    }

    /**
     * @param $f3
     * @throws \Exception
     */
    private function _guardAgainstFiltersOptionsNotFound($f3)
    {
        if (!$f3->exists('inputFilters')) {
            throw new \Exception('Variable "inputFilters" have not been found in validation config');
        }
    }
} 
<?php
namespace Main\Models;

use Models\Useful\ModelEngine;

class Account extends ModelEngine
{
    const STATE_ACTIVE = 1;
    const STATE_INACTIVE = 16;

    public static $fieldNamesCanBeUpdated = [
        'id', 'email', 'password', 'user_signature', 'user_id', 'company_id', 'auth_meta', 'auth_token', 'state'
    ];

    public static $standardInsertData = [
        'email'          => '',
//        'password'       => '',
        'user_signature' => '',
        'user_id'        => '',
        'company_id'     => '',
        'auth_meta'      => '',
        'auth_token'     => '',
        'state'          => self::STATE_ACTIVE,
    ];

    protected $_name = 'accounts';

    public function __construct($accountId = null)
    {
        parent::__construct($accountId);
    }
}
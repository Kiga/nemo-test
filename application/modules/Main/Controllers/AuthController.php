<?php
namespace Main\Controllers;

use Helpers as H;
use Base;
use Helpers\ShowJson;
use Helpers\MessageJumper;
use Models\User;
use Components\FormValidator;
use Components\PublicController;
use Components\Auth;

class AuthController extends PublicController
{
    /**
     * @var Auth
     */
    private $_authComponent;

    /**
     * @var MessageJumper
     */
    private $_jumper;

    /**
     * @var ShowJson
     */
    private $_jsonJumper;

    public function __construct()
    {
        parent::__construct();

        $this->_jumper        = MessageJumper::instance();
        $this->_jsonJumper    = ShowJson::instance();
        $this->_authComponent = Auth::instance();

        $this->_setLayout('lockscreen.mockup.htm');
    }

    public function showHash($f3, $params)
    {
        $uglySecret = '';
        if (!$f3->exists('PARAMS.check', $uglySecret) ||
            'sadfj9p8wefjhsp9o12ij3oep' != $uglySecret
        ) {
            die('bad');
        }

        $password = '';
        if (!$f3->exists('GET.pass', $password)) {
            die('no password');
            exit;
        }

        echo Auth::instance()->getPasswordProcessing($password);
    }

    /**
     * User login. Render form.
     */
    public function showLoginForm()
    {
        if ($formData = $this->_jumper->getData()) {
            $this->formData = $formData['data'];
        }
        Auth::instance()->clearSessionExeptsPrelogedData();

//        $this->_renderWithLayout('show-login-form');
        $this->_renderWithLayout('lockscreen');
    }

    /**
     * User login. Processing.
     *
     * @return mixed
     */
    public function login()
    {
        $f3               = Base::instance();
        $loginFormData    = $f3->get('POST');
        $inputFilter      = (new FormValidator('user-login'))->getFilter();
        $userPrelogedData = $this->_jumper->getData(null, MessageJumper::PRELOGED_CONTAINER_NAME);
        $inputFilter->setData($loginFormData);

        if (false == $inputFilter->isValid()) {
            $loginFormData['validatorMessages'] = $inputFilter->getMessages();

            return $this->_jumper->saveDataAndJump('@login_form', $loginFormData, 'Login failed. E-mail or passwords are invalid.', MessageJumper::RESULT_ERROR);
        }

        if (false == $this->_authComponent->login($inputFilter->getValue('email'), $inputFilter->getValue('password'))) {
            return $this->_jumper->saveDataAndJump('@login_form', $loginFormData, 'Login failed. E-mail or passwords are incorrect.', MessageJumper::RESULT_FAILURE);
        }

//        $userData = $this->_authComponent->getUserData();
        if (!empty($userPrelogedData['uri'])) {
            $userPrelogedData['hopes'] = 1; // the preloged data will be auto cleaned on next getting
            $f3->set(MessageJumper::PRELOGED_CONTAINER_NAME, $userPrelogedData);

            return $f3->reroute($userPrelogedData['uri']);
        }

        return $f3->reroute('@main_page');
    }

    public function loginAjax()
    {
        $f3          = Base::instance();
        $postData    = $f3->get('POST');
        $inputFilter = (new FormValidator('user-login'))->getFilter();
        $inputFilter->setData($postData);

        if (false == $inputFilter->isValid()) {
            return $this->_jsonJumper->answer(ShowJson::RESULT_ERROR, 'Login failed. E-mail or passwords are invalid.');
        }

        if (false == $this->_authComponent->login($inputFilter->getValue('email'), $inputFilter->getValue('password'))) {
            return $this->_jsonJumper->answer(ShowJson::RESULT_ERROR, 'Login failed. E-mail or passwords are incorrect.');
        }

        return $this->_jsonJumper->answer(ShowJson::RESULT_SUCCESS, 'Success');
    }

    public function showPasswordRecoveryForm()
    {
        $this->_renderWithLayout('show-recovery-form');
    }

    public function showRegistrationForm()
    {
        $this->_renderWithLayout('show-registration-form');
    }

    /**
     * User logout
     *
     * @return mixed
     */
    public function logout()
    {
        $this->_authComponent->logout();

        return Base::instance()->reroute('@main_page');
    }
}
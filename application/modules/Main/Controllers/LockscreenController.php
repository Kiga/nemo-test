<?php
namespace Main\Controllers;

use Components\PublicController;
use Components\FormValidator;
use Components\Auth;
use Helpers\ShowJson;
use Models\User;

class LockscreenController extends PublicController
{
    public function __construct()
    {
        parent::__construct();
        $this->_setLayout('lockscreen.mockup.htm');
    }

    public function index($f3, $params)
    {
//        $this->setScreenIsLocked(true);
        $this->_willBeRenderedWithLayout('index');
    }

    public function auth($f3, $params)
    {
        $postData = $f3->get('POST');
        $postData['email'] = 'access@access.com'; // hard code
        $inputFilter = (new FormValidator('user-login'))->getFilter();
        $inputFilter->setData($postData);

        if (false == $inputFilter->isValid()) {
            return ShowJson::instance()->answer(ShowJson::RESULT_ERROR, 'Login failed. Email or password are not valid.');
        }

        if (false == Auth::instance()->login($inputFilter->getValue('email'), $inputFilter->getValue('password'))) {
            return ShowJson::instance()->answer(ShowJson::RESULT_ERROR, 'Login failed. Incorrect email or password.');
        }

        $this->setScreenIsLocked(false);
        return ShowJson::instance()->answer(ShowJson::RESULT_SUCCESS, 'Success');
    }
}

<?php
namespace Main\Controllers;

use Components\PublicController;
use Zend\View\Helper\Placeholder\Registry;

/**
 * Class MainController
 * @package Controllers
 */
class MainController extends PublicController
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->_setLayout('main.mockup.htm');
        \Base::instance()->set('menuItem', 'main');
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    public function index($f3, $params)
    {
        $this->_willBeRenderedWithLayout('index');
    }
}

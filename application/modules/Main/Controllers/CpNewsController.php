<?php
namespace Main\Controllers;

use Components\FormValidator;
use Components\ProtectedController;
use Helpers\CssHtmlHelper;
use Helpers\MessageJumper;
use Models\News;

/**
 * Class CpNewsController
 * @package Main\Controllers
 */
class CpNewsController extends ProtectedController
{
    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->_setLayout('main.mockup.htm');
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    public function index($f3, $params)
    {
        return $this->showNews($f3, $params);
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    public function showNews($f3, $params)
    {
        $this->_willBeRenderedWithLayout('show-news');
    }

    /**
     * @param \Base $f3
     * @param $params
     */
    public function addNewsForm($f3, $params)
    {
        $f3->set('errorHelper', new CssHtmlHelper('formData'));
        if ($formData = MessageJumper::instance()->getData()) {
            $this->formData = $formData['data'];
        }

        $this->_willBeRenderedWithLayout('add-news-form');
    }

    /**
     * @param \Base $f3
     * @param $params
     * @return mixed
     */
    public function addNews($f3, $params)
    {
        $postData   = $f3->get('POST');
        $formFilter = (new FormValidator('news-add'))->getFilter();
        $formFilter->setData($postData);

        $jumpUrl = '/cp/news/add-news-form';
        if (false == $formFilter->isValid()) {
            $postData['validatorMessages'] = $formFilter->getMessages();

            return MessageJumper::instance()->saveDataAndJump($jumpUrl, $postData, 'Входные данные некорректны.', MessageJumper::RESULT_ERROR);
        }

        $news       = new News();
        $insertData = $news->addNewDocument($formFilter->getValues());

        return MessageJumper::instance()->saveDataAndJump($jumpUrl, [], 'Данные успешно добавлены.', MessageJumper::RESULT_SUCCESS);
    }
}
